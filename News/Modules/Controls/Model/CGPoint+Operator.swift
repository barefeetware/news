//
//  CGPoint+Operator.swift
//  News
//
//  Created by Tom Brodhurst-Hill on 9/4/19.
//  Copyright © 2019 BareFeetWare. All rights reserved.
//

import UIKit

func +(left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x + right.x,
                   y: left.y + right.y)
}

func +=(left: inout CGPoint, right: CGPoint) {
    left = left + right
}
