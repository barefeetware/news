//
//  StyledNavigationBar.swift
//  News
//
//  Created by Tom Brodhurst-Hill on 14/5/19.
//  Copyright © 2019 BareFeetWare. All rights reserved.
//

import UIKit

class StyledNavigationBar: UINavigationBar {

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    func commonInit() {
        titleTextAttributes = [NSAttributedString.Key.font: UIFont.title]
        tintColor = UIColor.NavigationBar.tint
        barTintColor = UIColor.NavigationBar.background
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [StyledNavigationBar.self])
            .setTitleTextAttributes([NSAttributedString.Key.font : UIFont.barButtonItem], for: .normal)
    }
    
}
