//
//  StyledButton.swift
//  News
//
//  Created by Tom Brodhurst-Hill on 9/4/19.
//  Copyright © 2019 BareFeetWare. All rights reserved.
//

import UIKit

@IBDesignable class StyledButton: UIButton {
    
    // MARK: - Enums
    
    enum Level: Int {
        case primary = 0
        case secondary = 1
        case tertiary = 2
        
        var backgroundColor: UIColor {
            switch self {
            case .primary: return .cerulean
            case .secondary: return .green
            case .tertiary: return .clear
            }
        }
        
        func titleColor(for state: UIControl.State) -> UIColor {
            switch self {
            case .primary, .secondary: return .white
            case .tertiary: return .cerulean
            }
        }
        
        var cornerRadius: CGFloat {
            switch self {
            case .primary, .secondary: return 4.0
            case .tertiary: return 0.0
            }
        }
    }
    
    // MARK: - Variables
    
    var level: Level = .primary {
        didSet {
            invalidateIntrinsicContentSize()
            backgroundColor = level.backgroundColor
            setTitleColor(level.titleColor(for: .normal), for: .normal)
            layer.cornerRadius = level.cornerRadius
        }
    }
    
    @IBInspectable var level_: Int {
        get {
            return level.rawValue
        }
        set {
            level = Level(rawValue: newValue) ?? .primary
        }
    }
    
    // MARK: - UIView
    
    override var intrinsicContentSize: CGSize {
        switch level {
        case .primary, .secondary: return CGSize(width: 400.0, height: 48.0)
        case .tertiary: return super.intrinsicContentSize
        }
    }
    
}
