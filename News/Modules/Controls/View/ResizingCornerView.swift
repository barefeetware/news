//
//  ResizingCornerView.swift
//  News
//
//  Created by Tom Brodhurst-Hill on 9/4/19.
//  Copyright © 2019 BareFeetWare. All rights reserved.
//

import UIKit

class ResizingCornerView: UIView {
    
    @IBOutlet var slaveView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    func commonInit() {
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(ResizingCornerView.move(_:)))
        addGestureRecognizer(panGestureRecognizer)
        backgroundColor = UIColor.red.withAlphaComponent(0.5)
    }
    
    @objc func move(_ sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: self)
        frame.origin += translation
        slaveView.frame.size.width = frame.maxX
        slaveView.frame.size.height = frame.maxY
        sender.setTranslation(.zero, in: self)
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: 84.0, height: 44.0)
    }
    
}
