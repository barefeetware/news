//  Copyright © 2019 Nine. All rights reserved.

import UIKit

extension UIColor {
    
    // MARK: - Raw colors
    
    static let darkIndigo = UIColor(red: 10.0 / 255.0, green: 22.0 / 255.0, blue: 51.0 / 255.0, alpha: 1.0)
    static let dullGreen = UIColor(red: 184.0 / 255.0, green: 233.0 / 255.0, blue: 134.0 / 255.0, alpha: 1.0)
    static let cerulean = UIColor(red: 9.0/255.0, green: 109.0/255.0, blue: 210.0/255.0, alpha: 1.0)
    static let lightGreen = UIColor(red: 184.0 / 255.0, green: 233.0 / 255.0, blue: 134.0 / 255.0, alpha: 1.0)
    // TODO: Adjust saturation and/or brightness instead, since rendering alpha is slower and changes according to background.
    static let lightSkyBlue = UIColor.skyBlue.withAlphaComponent(0.5)
    static let scarlet = UIColor(red: 208.0 / 255.0, green: 2.0 / 255.0, blue: 27.0 / 255.0, alpha: 1.0)
    static let skyBlue = UIColor(red: 88.0 / 255.0, green: 181.0 / 255.0, blue: 1.0, alpha: 1.0)
    static let warmGray = UIColor(white: 112.0 / 255.0, alpha: 1.0)
    
    // MARK: - Derived colors
    
    struct Text {
        static let normal = UIColor.black
        static let secondary = UIColor.warmGray
    }
    
    struct NavigationBar {
        static let background = UIColor.white
        static let tint = UIColor.black
    }
    
    struct Table {
        static let separator = UIColor.lightGray
        
        struct SectionHeader {
            static let background = UIColor.white
        }
    }
    
}
