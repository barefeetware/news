//  Copyright © 2019 Nine. All rights reserved.

import UIKit

extension UIFont {
    
    // MARK: - Base fonts
    
    static func ptSans(size: CGFloat) -> UIFont {
        return UIFont(familyName: "PT Sans", size: size)
    }
    
    static func ptSerif(size: CGFloat) -> UIFont {
        return UIFont(familyName: "PT Serif", size: size)
    }
    
    // MARK: - Purpose fonts, mapped to base fonts
    
    // TODO: Add color: 46, 46, 46
    static let body = UIFont.ptSerif(size: 17.0)
    static let title = UIFont.ptSans(size: 17.0)
    static let heading = UIFont.ptSans(size: 13.0).bold
    static let barButtonItem = title
    
}
